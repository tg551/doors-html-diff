class DoorsFieldFactory
  def self.get_field heading, text
    
    chosen_doorsfield = case heading
    when false then nil
    when :Main then MainDoorsField
    when :" Links " then LinksDoorsField
    else DoorsField
    end
    
    chosen_doorsfield.new text
  end
end

class DoorsField
  attr_accessor :text
  
  def initialize field_html
    @text = field_html
  end
  
  def ==(other)
    @text == other.text 
  end
  
  def html_diff new_thing
    #there seems to be a bug with ignoring whitespace on linux, so provide
    #different arguments to diff on windows and linux
    unless RbConfig::CONFIG["arch"].include? "linux"
      Diffy::Diff.new(@text, new_thing.text, diff: "-U 10000 -w").to_s(:html)
    else
      Diffy::Diff.new(@text, new_thing.text).to_s(:html)
    end
  end
end

class DoorsFieldForIgnoringChanges < DoorsField
  
  def different_to? other
    false
  end
  
  def html_diff other
    ""
  end
end

class MainDoorsField < DoorsField
  def initialize text
    super
    calculate_deletedness
  end
  
  private
  
  def calculate_deletedness
    @deleted = !!(@text =~ /Deleted\.?/i)
  end
end

class LinksDoorsField < DoorsField
  def initialize text
    super
    @text = @text.scan(/\(\S+\)\(OUT\)/).sort_by {|link| link[/\d+/].to_i}.join " "
  end
end