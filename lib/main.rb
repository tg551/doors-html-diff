start = Time.now
require 'nokogiri'
require 'pathname'
require 'set'
require 'find'
require 'date'
require 'diffy'
require 'pp'

require './doorsdocument'
require './documentset'
require './doorsfields'
require './argloader'

args = load_args ARGV.first

documents = DocumentSet.new args[:exports]

documents.make_diff_file(args[:output])

_end = Time.now
warn "\nit took #{_end - start} seconds"
