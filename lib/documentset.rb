class DocumentSet
  def initialize exports_location
    exports = exports_location
    @documents = exports.map {|export| DoorsDocument.new export}
  end
  
  def make_diff_file output_location
    every_req = get_all_requirement_numbers
    print_what_is_being_compared_and_what_isnt shared_fields
    
    if shared_fields.empty?
      puts "there are no shared fields between any exports. There is nothing to do"
      exit
    end
    
    html = initialize_html_output
    body = html.at_xpath "/html/body"
  
    every_req.each do |req|
      body.add_child "<h1>#{req}</h1>"
      print "\r" + req + " " * 20
      @documents.each_cons(2) do |old, new|
        if old.has_req?(req) && !new.has_req?(req)
          body.add_child "Object not in export #{new.document_name}\n\n"
        elsif new.has_req?(req) && !old.has_req?(req)
          body.add_child("Object new in export #{new.document_name}\n\n")
        end
      end
      
      shared_fields.each do |field|
        @documents.each_cons(2) do |old_doc, new_doc|
          next if old_doc.has_req?(req) && !new_doc.has_req?(req)
          next if new_doc.has_req?(req) && !old_doc.has_req?(req)
          next if !(new_doc.has_req? req) && !(old_doc.has_req? req)
          
          unless old_doc.field_different_to?(req, field, new_doc)
            #body.add_child old_doc.field(req, field).html_diff(new_doc.field req, field)
            body.add_child old_doc.html_diff(req, field, new_doc)
          else
          end
        end
      end
    end
  
    File.write output_location, html.to_html
  end
  
  private 
  
  def print_what_is_being_compared_and_what_isnt documents
    puts "comparing on the following fields"
    puts shared_fields
    puts
    puts
    puts "not comparing on the following fields:"
    non_shared = non_shared_fields
    puts(if non_shared.empty? then "(none)" else non_shared end)
    puts
  end
 
  
  def enumerate_exports dir
    Find.find(dir).map {|d| Pathname.new d}.select {|d| d.extname.downcase == ".htm"}.sort {|this, other| date_and_baseline_cmp(this, other)}
  end
  
  def shared_fields
    @documents.map(&:fields).reduce(:&)
  end
  
  def non_shared_fields
    @documents.map(&:fields).reduce(:|) - shared_fields
  end
  
  def get_all_requirement_numbers
    Set.new @documents.reverse.map {|d| d.object_identifiers}.flatten
  end
  
  def initialize_html_output
    Nokogiri::HTML.parse File.read("template")
  end
end
