class DoorsDocument
  attr_reader :document_name
  def initialize location  
    find_document_name location
    html_file_contents = File.read(location, encoding: "windows-1252").encode("utf-8", invalid: :replace, undef: :replace).gsub("&nbsp;", "\u00A0")
    html_document = Nokogiri::HTML html_file_contents
    @contents = dataify html_document
  end
  
  def fields
    @contents.values.first.keys
  end
  
  def object_identifiers
    @contents.keys
  end
  
  def has_req? req
    @contents.keys.include? req
  end
  
  def values object_identifier
    begin
      @contents.fetch object_identifier
    rescue
      Hash.new("")
    end
  end
  
  def field object_identifier, field_name
    @contents.fetch(object_identifier).fetch(field_name)
  end
  
  def field_different_to? object_identifier, field_name, other_document
    this_field = field object_identifier, field_name
    other_field = other_document.field object_identifier, field_name
    this_field == other_field
  end
  
  def html_diff object_identifier, field_name, other_document
    this_field = field object_identifier, field_name
    other_field = other_document.field object_identifier, field_name
    
    title = "<h3>#{field_name.to_s}: #{document_name.strip} -> #{other_document.document_name.strip}</h3>"
    diff = this_field.html_diff other_field
    Nokogiri::HTML::DocumentFragment.parse title + "\n" + diff
  end
  
  def object_identifiers
    @contents.keys
  end
 
  def specific_values field_name
    @contents.each_pair.with_object({}) do |(req_name, contents_hash), ret|
      ret[req_name] = contents_hash[field_name]
    end
  end
  
  private
  
  def find_document_name location
    begin
      @document_name = Pathname.new(location).parent.basename.to_s
    rescue
      @document_name = nil
    end
    
    @document_name = location if @document_name.nil?
    @document_name
  end
  
  def dataify html_document
    html_document.css("br").each {|node| node.replace("\n")} # preserve newlines!
    table = extract_table html_document
    headings = extract_headings table
    rows = extract_rows table
    rows_of_fields = rows.map {|r| extract_fields r}
    
    acc = {}
    rows_of_fields.each do |fields|
      
      object_identifier_found = false
      object_identifier = nil
      contents_hash = {}
      
      headings.zip(fields) do |heading, value| 
        doors_field = DoorsFieldFactory.get_field(heading, value)
        contents_hash[heading] = doors_field
        if heading == :"Object Identifier"
          object_identifier_found = true
          object_identifier = value
        end
      end
     
      raise unless object_identifier_found
      acc[object_identifier] = contents_hash
    end
    return acc
  end
  
  def extract_table html_document
    html_document.xpath "/html/body/table"
  end

  def extract_rows table_element
    table_element.xpath("//tr")[1..-1]
  end

  def extract_fields table_row
    table_row.xpath("td").map &:text
  end

  def extract_headings table_element
    table_element.xpath("//tr/th").map(&:text).map &:to_sym
  end
end