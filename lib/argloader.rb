require 'yaml'

def load_args location
  raise "arguments missing" if location.nil?
  YAML::load File.read location
end